# The GNOME 3.28 18.04 runtime snap

This snap includes a GNOME 3.28 stack (the base libraries and desktop integration components) and shares it through the content interface. It's built using the Ubuntu 18.04 binaries.

## How to use this snap

The easiest way to use this runtime in your snap is [the gnome-3-28 extension](https://forum.snapcraft.io/t/the-gnome-3-28-extension/13485).

## Reporting bugs

Important! Only report issues here if they are about *the runtime snap*. **If you have general issues about GNOME or GNOME applications on Ubuntu, follow the instructions in the ["reporting bugs in Ubuntu" guide](https://help.ubuntu.com/community/ReportingBugs)**

## Need help?

If you need help using this runtime snap in your applications, feel free to ask questions in the [Snapcraft Forum](https://forum.snapcraft.io) or on the [Snapcraft Rocketchat](https://rocket.ubuntu.com/channel/snapcraft).
